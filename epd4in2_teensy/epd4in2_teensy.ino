/* El cheapo barograph*/

#include <SPI.h>
#include <Wire.h>
#include <TimeLib.h>
#include <EEPROM.h>
#include <BMP388_DEV.h>   
#include "epd4in2.h"
#include "epdpaint.h"

BMP388_DEV bmp388; 
Epd epd;
time_t RTCTime;                
                        
#define COLORED     0          // for epd
#define UNCOLORED   1          // for epd
#define num_avgs 144           // 48 hours of data at 20 min intervals
#define data_interval 20       // in minutes

float last_update;             // time of last average in the avgs array
int16_t avgs[num_avgs] = {0};  // array of avg pressures 
bool updated = false;          // flag so we don't update the display twice in a single minute
int min_pressure = 950;        // for graph (in mb)
int max_pressure = 1050;       // for graph (in mb)
float temperature, pressure, altitudem; // instantaneous values
float temperature_sum, pressure_sum;    // for averaging
int counter;                            // for averaging
unsigned char image[16000];             // array for full epd image (400x300)   
    
Paint paint(image, 400, 300);  //create epd image


void setup() {
  Serial.begin(115200);

   if (! bmp388.begin()) {
    Serial.println("Couldnt find sensor");
    return;  
  }

  if (epd.Init() != 0) {
    Serial.print("e-Paper init failed");
    return;
  }

  /* get time */
  setSyncProvider(getTeensy3Time);
  
  /* set up the epd, clear the SRAM and say Hai */
  paint.SetWidth(400);
  paint.SetHeight(300);
  epd.ClearFrame();
  paint.Clear(UNCOLORED);
  paint.DrawStringAt(175, 100, "Hi!", &Font24, COLORED);
  epd.SetPartialWindow(paint.GetImage(), 0, 0, paint.GetWidth(), paint.GetHeight());
  epd.DisplayFrame();

  /* get current pressure and temp values when sensor is ready */
  bmp388.startForcedConversion(); // Start BMP388 in forced conversion mode
  do {
    delay(100);
  } while (! bmp388.getMeasurements(temperature, pressure, altitudem));

  /* retreive time and avg pressure arrays from EEPROM and adjust for powered down time */
  readFromEEPROM();
  updateAvgsArray();
  
  /* update display */
  updateDisplay(int(round(pressure)),int(round(temperature)));

}

void loop() {

  /*reset update flag */
  if ((minute() % data_interval != 0) && updated==true) {
    updated = false;
  }

  /* take readings every second */
  bmp388.startForcedConversion();                 // Start BMP388 in forced conversion mode
  delay(1000);                                   
  if (bmp388.getMeasurements(temperature, pressure, altitudem)) {
    Serial.println(temperature);
    Serial.println(pressure);
    Serial.println();
    pressure_sum = pressure_sum + pressure;
    temperature_sum = temperature_sum + temperature;
    counter++;
  }
 
  /* if we're an increment of data_interval then update the display*/
  if ((minute() % data_interval == 0) && updated==false) {
    Serial.println("updating");
    /* set flag so we don't repeat the update within this minute*/
    updated = true;
    
    /*calculate averages and reset counters*/
    int avg_press = int(round(pressure_sum/float(counter)));
    int avg_temp  = int(round(temperature_sum/float(counter)));
    counter = 0;
    pressure_sum = 0;
    temperature_sum = 0;

    /*update timestamp and pressure array for plotting and for saving to EEPROM*/
    last_update = now();
    shiftArray(avg_press);
    
    /* write to EEPROM on the hour */
    if (minute()==0) {
      writeToEEPROM();
    }

    /* update display*/
    updateDisplay(avg_press,avg_temp);
    
  }
}

void updateDisplay(int pressure, int temperature) {
  
    /* wake epd up */
    epd.Init();
    epd.ClearFrame();

    /* format time and date */
    char time_char[6];
    sprintf(time_char, "%02d:%02d", hour(), minute());
    char date_char[6];
    sprintf(date_char, "%02i/%02i", month(), day());

    /* format avg pressure and temperature */
    char pres_char[5];
    sprintf(pres_char, "%4i", pressure);
    char temp_char[4];
    sprintf(temp_char, "%3i", temperature);
   
    /* Draw current values*/
    paint.Clear(UNCOLORED);
    paint.DrawStringAt(53, 5, pres_char, &Font72, COLORED);
    paint.DrawStringAt(strlen(temp_char)+173, 28, "mb", &Font24, COLORED);
    paint.DrawStringAt(240, 5, temp_char, &Font72, COLORED);
    paint.DrawStringAt(strlen(temp_char)+328, 28, "C", &Font24, COLORED);

     /* draw y-axis */
    int xax_scale = 2;
    int yax_scale = 2;
    int yax_height = yax_scale*(max_pressure-min_pressure);
    int yax_xpos = 60;
    int yax_ypos = 67;
    int txt_vert_offset = 6;
    int xax_length = 288; 
    int xax_xpos = yax_xpos;
    int xax_ypos = yax_ypos+yax_height;

    for (int i=0; i<=10; i++){
      char text[4];
      dtostrf((10*i+min_pressure),4,0,text);                                      //create values for y-axis tick labels
      int yax_tick = yax_ypos-txt_vert_offset+20*(10-i);                          //create y-axis ticks
      paint.DrawStringAt(yax_xpos-36, yax_tick, text, &Font12, COLORED);          //draw y-axis tick labels
      paint.DrawDottedHorizontalLine(yax_xpos,yax_tick+txt_vert_offset, xax_length, COLORED); //draw y-axis ticks
    }  

     /* draw x-axis */
    paint.DrawHorizontalLine(xax_xpos, xax_ypos,   xax_length, COLORED);   //x-axis line
    paint.DrawHorizontalLine(xax_xpos, xax_ypos+1, xax_length, COLORED);   //x-axis line
    paint.DrawStringAt(xax_xpos+xax_length/2-40, xax_ypos+txt_vert_offset+12, "Hours",   &Font12, COLORED); //draw x-axis label
    paint.DrawStringAt(xax_xpos+xax_length-16,   xax_ypos+txt_vert_offset,    time_char, &Font12, COLORED); //last update time 
    paint.DrawStringAt(xax_xpos+xax_length-16,   xax_ypos+txt_vert_offset+12, date_char, &Font12, COLORED); //last update date 
    paint.DrawDottedVerticalLine(xax_xpos+xax_length,xax_ypos+1-200, 203, COLORED);    //draw date and time tick/line
    for (int i=1; i<=8; i++){
      char text[4];
      sprintf(text, "%2i", -6*i);                                                        //create values for x-axis tick labels
      int xax_tick = xax_xpos+xax_length-36*i;                                           //create x-axis ticks
      paint.DrawStringAt(xax_tick-11, xax_ypos+txt_vert_offset, text, &Font12, COLORED); //draw x-axis tick labels
      paint.DrawDottedVerticalLine(xax_tick,xax_ypos+1-200, 203, COLORED);               //draw x-axis ticks/lines
    }  

    /* draw graph */
    for(int i = 0; i < num_avgs; i++) {
      if (avgs[i] > 0) {
        int yval = yax_scale*(max_pressure-avgs[i])+yax_ypos;
        int xval = (xax_scale*i)+yax_xpos+1;
        paint.DrawPixel(xval   ,yval   ,COLORED);  // each data point is 2x2 pixels, drawing the individually is faster than drawing a square
        paint.DrawPixel(xval   ,yval+1 ,COLORED);
        paint.DrawPixel(xval-1 ,yval+1 ,COLORED);
        paint.DrawPixel(xval-1 ,yval   ,COLORED);
      }
    }

    /* output entire image to epd */
    epd.SetPartialWindow(paint.GetImage(), 0, 0, paint.GetWidth(), paint.GetHeight());
    epd.DisplayFrame();

    /* put epd to sleep */
    epd.Sleep();
}

void writeToEEPROM() {
  EEPROM.put( 0, last_update );
  EEPROM.put( 4 , avgs );
}

void readFromEEPROM() {
  EEPROM.get( 0, last_update );
  EEPROM.get( 4 , avgs );
}

void updateAvgsArray() {

  /* calculate how long it's been since we stopped getting data */
  float down_time = (now() - last_update)/60.0;   // minutes since last update 
  int dataOffset  = int(down_time/data_interval); // data intervals since last update
  Serial.println(down_time);

  /* If clock has reset to 01/01, erase all data and start over */
  if (now() < 1577836800) {
    dataOffset = 146;
  }

  /* update avgs array with zeros for missed intervals. Since we only write to EEPROM every hour, there will ususally be missed intervals*/
  int j = 0;
  while (j<(dataOffset)){
    shiftArray(0);
    j++;
  }
  
}


void shiftArray(int avg_press) {
  
  /* shift avgs array by one and add newest values */ 
  int i =0;
  while (i<(num_avgs-1)){
    avgs[i]  = avgs[i+1];   //oldest reading is at index 0 and newest is at last index
    i++;
  }
  avgs[(num_avgs-1)]  = avg_press; //add newest avg to array
  
}


time_t getTeensy3Time() {
  return Teensy3Clock.get();
}
